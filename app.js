var express = require("express");
var app     = express();
var path    = require("path");
const PORT = process.env.PORT || 5000

app.use(express.static(__dirname + '/assets'));

app.get('/',function(req,res){
  res.sendFile(path.join(__dirname+'/index.html'));
});

app.get('/about',function(req,res){
  res.sendFile(path.join(__dirname+'/about.html'));
});

app.get('/sitemap',function(req,res){
  res.sendFile(path.join(__dirname+'/sitemap.html'));
});

app.listen(PORT);

console.log("Running at Port 8000");
